# telegram-media-decoder

It tries to decode media from telegram app.
Required: access to /data/data/org.telegram.messenger in order to retrieve the media.ext.enc.key

## Usage
From command line:
```bash
./tg-decode.py --media ./-5870902822990626839_121.jpg.enc --key ./-5870902822990626839_121.jpg.enc.key
```
A new file with appropriate name will appear next to the encoded file.

## Authors and acknowledgment
Jybz

## License
GPLv3

## Project status
Not working.

## Source
Few reference URL:
https://github.com/clostra/Telegram-Android/blob/master/TMessagesProj/src/main/java/org/telegram/messenger/secretmedia/EncryptedFileDataSource.java#L65C1-L65C1
https://github.com/clostra/Telegram-Android/blob/master/TMessagesProj/src/main/java/org/telegram/messenger/secretmedia/EncryptedFileDataSource.java#L96
https://github.com/TelegramOrg/Telegram-Android/blob/master/TMessagesProj/src/main/java/org/telegram/messenger/Utilities.java#L70
https://pycryptodome.readthedocs.io/en/latest/src/cipher/classic.html#ctr-mode
https://pycryptodome.readthedocs.io/en/latest/src/util/util.html#Crypto.Util.Counter.new
