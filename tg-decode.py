#!/bin/python

import argparse
from pathlib import Path
from Crypto.Cipher import AES
from Crypto.Util import Counter

def main():
    parser = argparse.ArgumentParser(description='Decode telegram coded media with key.')
    parser.add_argument('--media', dest='media_file', nargs=1, type=Path, required=True, help='path to the encoded media')
    parser.add_argument('--key',   dest='key_file',   nargs=1, type=Path, required=True, help='path to the key file')

    args = parser.parse_args()
    
    media_file_decoded = Path(args.media_file[0].parents[0].resolve() / args.media_file[0].stem)
    
    with args.key_file[0].open('br') as key_file:
        key_file_content = key_file.read()
        key = key_file_content[0:32]
        iv = key_file_content[32:48]
        iv_to_number = int.from_bytes(iv, 'big', signed=False)
        with args.media_file[0].open('br') as media_file:
            media_file_content = media_file.read()
            counter = Counter.new(len(iv), initial_value=iv_to_number, little_endian=False)
            cipher = AES.new(key=key, mode=AES.MODE_CTR, counter=counter)
            try:
                decoded_media_content = cipher.decrypt(media_file_content)
                with media_file_decoded.open('bw') as decoded_file:
                    decoded_file.write(decoded_media_content)
            except (ValueError, KeyError):
                print("Incorrect decryption")

if __name__ == "__main__":
    main()

